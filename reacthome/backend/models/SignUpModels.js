const mongose = require('mongoose')

const signUpTemplate = new mongose.Schema({
   

    username:{
        type:String,
        required:true
    },

    email:{
        type: String,
        required:true
    },
    password:{
        type:String,
        required:true
    },

    password2:{
        type:String,
        required:true
    },

    date:{
        type:Date,
        default:Date.now
    }

})

module.exports = mongose.model('HomeApp', signUpTemplate)
const express = require('express')
const app = express()
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const routesUrls = require('./routes/routes')
const cors = require('cors') 

dotenv.config()

mongoose.connect(process.env.DATABASE_ACCESS, ()=>console.log("Database connected successfully"))
//mongoose.connect(process.env.DATABASE_ACCESS2, ()=>console.log("Database connected successfully mytable"))
//mongoose.connect(process.env.DATABASE_ACCESS3, ()=>console.log("Database connected successfully tasks"))
app.use(express.json())
app.use(cors())
app.use('/app', routesUrls)
app.use('/app2', routesUrls)
app.use('/app3', routesUrls)
app.use('/app4', routesUrls)
app.listen(4000, ()=> console.log("server is up"))
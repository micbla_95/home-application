const express = require('express')
const router = express.Router()
const signUpTemplateCopy = require('../models/SignUpModels')
const addProductTemplateCopy = require('../models/ProductModels')
const addTaskTemplateCopy = require('../models/ToDoModel')
const addDutyTemplateCopy = require('../models/ScheduleModel')



// create account
router.post('/signup', (request, response) =>{
    const signedUpUser = new signUpTemplateCopy({
        
        username:request.body.username,
        email:request.body.email,
        password:request.body.password,
        password2:request.body.password2,
        
    })

    signedUpUser.save()
    .then(data =>{
        response.json(data)
    })

    .catch(error =>{
        response.json(error)
    })
})
module.exports = router






//add product
router.post('/products', (request2, response2) =>{
    const addProduct = new addProductTemplateCopy({
        
        product:request2.body.product,

        
    })

addProduct.save()
    .then(data =>{
        response2.json(data)
    })

    .catch(error =>{
        response2.json(error)
    })
})
module.exports = router



//add task

router.post('/task', (request3, response3) =>{
    const addTask = new addTaskTemplateCopy({
        
        task:request3.body.task,
        //ddl:request3.body.date,
 
        
    })

    addTask.save()
    .then(data =>{
        response3.json(data)
    })

    .catch(error =>{
        response3.json(error)
    })
})
module.exports = router


// add duty

router.post('/duty', (request4, response4) =>{
    const addDuty = new addDutyTemplateCopy({
        
        who:request4.body.who,
        place:request4.body.place,
        
    })

    addDuty.save()
    .then(data =>{
        response4.json(data)
    })

    .catch(error =>{
        response4.json(error)
    })
})
module.exports = router


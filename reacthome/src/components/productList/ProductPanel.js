import React, {Component} from 'react'
import AddProduct from './AddProduct';
import ProductList from './ProductList';
import Navigation from '../Navigation.js';

class ProductPanel extends Component {
    productCounter = 0;
    state = { 
        products : [


        ]

     }

     deleteProduct = (id)=>{
        const products = [...this.state.products];
        const index = products.findIndex(product =>product.id === id);
        products.splice(index,1);
  
        this.setState({
          products
        })

     }


     AddProduct = (text)=>{
        const product={
            id:this.productCounter,
            text: text,
            active:true,
           }
           this.productCounter++

        this.setState(prevState=>({
            products:[...prevState.products, product]
          }))
         return true

     }



    render() { 
        return (  
            <>
           
            <div class="ProductPanel">
                <AddProduct add = {this.AddProduct}/>
                <ProductList products={this.state.products} 
         delete={this.deleteProduct}/>
            </div>
            </>
        );
    }
}
 
export default ProductPanel;
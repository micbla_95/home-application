import Product from './Product'

 
const ProductList = (props) => {
    const active = props.products.filter(product => product.active);
    const activeProducts = active.map(product=> <Product key={product.id} product={product}
      delete={props.delete} />)
    
      
        return ( 
            
            <div class="active">
            <h2> Products</h2>
              {activeProducts}
            </div>
              
             );
    }
     
    export default ProductList;
import React, {Component} from 'react'
import axios from 'axios'
import ProductList from './ProductList'
class AddProduct extends Component {
    state = { 
        text: '',
        displayText: []
    
     }

     componentDidMount=()=>{
         this.getProduct()
     }

     getProduct = () =>{
         axios.get('http://localhost:4000/app2/products')
         .then((response)=>{
             const data = response.data;
             this.setState({
                 displayText:data
             })
             console.log('Data has benn received')
         })
         .catch(()=>{
             console.log('Data hasn"t been received')
         })
     }

     handleText=(e)=>{
         this.setState({
             text:e.target.value
         })
     }
   

     handleClick = (e)=>{
        e.preventDefault()
        const items={
           product: this.state.text,
           
        }
        
        axios.post('http://localhost:4000/app2/products', items)
        .then(response2 => console.log(response2.data))

         const {text} = this.state
         const add = this.props.add(text)
         if(add){
             this.setState({
                 text: ''
              
             })
         }
     }


       displayProduct=(displayText)=>{
            if(!displayText.lenght) return null

            return displayText.map((displayText,index)=>(
                <div key={index}>
                    <h3>{displayText.text}</h3>
                    <p>{displayText.body}</p>

                </div>
            ))
       }

    render() { 
        return ( 
            <div className="form">
            <input type="text" placeholder="Add product do list" 
            value={this.state.text} onChange={this.handleText}/>
            <button onClick={this.handleClick}>Add</button>
         
         <div className="blog">
        {this.displayProduct(this.state.displayText)}
         </div>
     
         </div> 
         );
    }
}
 
export default AddProduct;
import React from 'react';


const Product = (props) => {

    const {text,id}=props.product
    return ( 
        <div>
           <p>
               {text}
            
                <button onClick={()=>props.delete(id)}>X</button>
           </p>
        </div>
     );
}
 
export default Product;
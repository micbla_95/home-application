import React, {Component} from 'react';
import { useHistory } from "react-router-dom";
import { withRouter } from 'react-router-dom'
import { Redirect } from "react-router-dom";
import axios from 'axios'
import App from './App'
class  SignIn extends Component {
 
    
    state = {  
        checkPassword:"aaa",
        checkMail: "bbb",
        email: '',
        password: '',
     
        //signinInfo:true
    }

  handleEmailInput=(e)=>{
    this.setState({
        email:e.target.value
    })
  }

  handlePasswordInput=(e)=>{
    this.setState({
        password:e.target.value
    })
  }



  submitLogin=(e)=>{
  

    this.setState({
        email:this.state.email,
        password:this.state.password
       
    })

    if((this.state.email === this.state.checkMail) && (this.state.password === this.state.checkPassword) )
    {
    

     window.location.assign('/productList/ProductPanel.js')



    console.log(`${this.state.email}`)
    console.log(`${this.state.password}`)
    console.log(`${this.state.signinInfo}`)

    }
  }
        
        
    render() { 
        return (  
            <>
          
           <h1>Sign in</h1>
              <div class="social-container">
              <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
              </div>
              <span>or use your account</span>
			        <input type="email" placeholder="Email"
                    onChange={this.handleEmailInput}
                    value={this.state.email}
                    />
			        <input type="password" placeholder="Password" 
                        onChange={this.handlePasswordInput}
                     value={this.state.password}
                    
                    />
			        <a href="#">Forgot your password?</a>
			        <button onClick={this.submitLogin}>Sign In</button>
            </>
        );
    }
}
 
export default withRouter(SignIn) ;
import React, {Component} from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Link } from 'react-router-dom';
import Navigation from './Navigation.js';
import Schedule from './schedule/Schedule.js';
import Todo from './to_do_list/Todo';
import '../styles/App.css';
import BG from "./bg.mp4"
import Panel from './Panel.js';

import SplitPanel from './split/SplitPanel.js';
import ProductPanel from './productList/ProductPanel.js'
import {  Route } from "react-router";
class App extends Component {
  state = {   

  
  }

checkIfMainRoute(){
  return window.location.pathname !== "/"
  
}



  render() { 



    return ( 


      
       <Router>

       
    <div className="App">
    <Route path="/" render={() => this.checkIfMainRoute() ? < Navigation /> : null} />

      <div className="text-center">
        <div className="children">
          <div className="container">
           
            <Route exact path="/" component={Panel} />
             
             <Route path="/to_do_list/Todo.js" component={Todo} />
             <Route path="/productList/ProductPanel.js" component={ProductPanel} />
             <Route path="/split/SplitPanel.js" component={SplitPanel} />
             <Route path="/schedule/Schedule.js" component={Schedule} />
             
             
          
          </div>
        </div>

      
      </div>
    </div>
  </Router>
     );
  }
}
 
export default App;
//import React from 'react';
import React, {Component} from 'react';
import RightPanelInfo from './RightPanelInfo.js';
import LeftPanelInfo from './LeftPanelInfo.js';
import CreateAccount from './CreateAccount.js';
import SignIn from './SignIn.js';
import '../styles/Panel.css';

class Panel extends Component {
  state = { 
    visible:true,
    visible2:false,
    PanelVisible:true 
 
   }

changePanel=(visible,visible2)=>{
  this.setState(previousState => {
    return {
      visible: !previousState.visible,
      visible2: !previousState.visible2 
    }
  })
}

signInAction=(PanelVisible)=>{
  this.setState(previousState => {
    return {
      PanelVisible: !previousState.PanelVisible,
    
    }
  })
}




  render(){
    return ( 
       <>
       
      <div className="panel">
        <div className="container">
          
          <div className="form-container sign-up-container">
           

          </div>

          <div className="form-container sign-in-container">
          {this.state.visible2 ? <LeftPanelInfo changeLeft={this.changePanel }/> : <SignIn signInAction={this.signInAction}/>} 

          </div>


          <div className="overlay-container">
		        <div className="overlay">

			        <div className="overlay-panel overlay-left">
            
		          </div>
              


			        <div className="overlay-panel overlay-right">
				       {this.state.visible ? <RightPanelInfo  changeRight={this.changePanel} /> : <CreateAccount/>} 
		          </div>

		        </div>
	        </div>
        </div>
        </div>
        </>
     );

    }

}
 
export default Panel;



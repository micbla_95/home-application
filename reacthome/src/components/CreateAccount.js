import React, {Component} from 'react';
import axios from 'axios'
class  CreateAccount extends Component {
    
    state = { 
        username: '',
        email:'',
        password:'',
        password2:'',
        
     }

     changeUsername=(e)=>{
        this.setState({
            username:e.target.value
        })
    }
        changeEmail=(e)=>{
            this.setState({
                email:e.target.value
            })

        }
            changePassword=(e)=>{
                this.setState({
                    password:e.target.value
                })
            }


            changePassword2=(e)=>{
                this.setState({
                    password2:e.target.value
                })
            }

            onSubmit=(e)=>{
                e.preventDefault()
                const registered={
                
                    username: this.state.username,
                    email: this.state.email,
                    password: this.state.password,
                    password2: this.state.password2
                }

               axios.post('http://localhost:4000/app/signup', registered)
                .then(response => console.log(response.data))

                this.setState({
                    username:'',
                    email:'',
                    password:'',
                    password2:''
                })

             console.log("hhhh")
            }
        
    render() { 
        return (  
            <>
             <div className='container'>
                <h1>Create Account</h1>
              <div class="social-container">
              <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
              </div>
              <form onSubmit={this.onSubmit}>
              
              <input type="text"
               placeholder="Name" 
               onChange={this.changeUsername}
               value={this.state.username}
               />
			    
                
             <input type="email"
              placeholder="Email"
              onChange={this.changeEmail}
              value={this.state.email}
              />
			       
                   
              <input type="password" 
              placeholder="Password" 
              onChange={this.changePassword}
             value={this.state.password}
              />



              <input type="password" 
              placeholder="Repeat Password"
              onChange={this.changePassword2}
              value={this.state.password2}
              />


                <input type='submit' className='btn btn-danger btn-block' value='Submit'/>
       
              </form>
              </div>
            </>
        );
    }
}
 
export default  CreateAccount  ;
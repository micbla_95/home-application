import React, {Component} from 'react'
import axios from 'axios'
class AddDuty extends Component {
    minDate = new Date().toISOString().slice(0,10);
    state = { 
        text: '',
        text2:'',
        date:this.minDate
    
     }

     handleText=(e)=>{
         this.setState({
             text:e.target.value,
           
         })
     }

     handleText2=(e)=>{
        this.setState({
            text2:e.target.value,
          
        })
    }

     handleDate = (e) =>{
        this.setState({
            date: e.target.value
        })
     }
   

     handleClick = (e)=>{
        e.preventDefault()

        const duty={
            who: this.state.text,
            place: this.state.text2
            
         }


         axios.post('http://localhost:4000/app4/duty', duty)
         .then(response4 => console.log(response4.data))

         const {text,text2,date} = this.state
         const add = this.props.add(text,text2,date)
 
         if(add){
             this.setState({
                 text: '',
                 text2: '',
                 date: this.minDate
              
             })
         }


     }
    render() { 

        let maxDate = this.minDate.slice(0, 4)*1 +1;
        maxDate = maxDate + "-12-31";
        return ( 
            <div className="form">

            <input type="text" placeholder="Add product do list2" 
            value={this.state.text} onChange={this.handleText}/>
            <input type="text" placeholder="Add product do list" 
            value={this.state.text2} onChange={this.handleText2}/>
              < input type="date" value={this.state.date} min= {this.minDate} 
               max={maxDate} onChange={this.handleDate}/>
            <button onClick={this.handleClick}>Add</button>         
         </div> 
         );
    }
}
 
export default AddDuty;
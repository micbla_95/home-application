import Duty from './Duty'

 
const Duties = (props) => {
    const active = props.duties.filter(duty => duty.active);
    const done = props.duties.filter(duty => !duty.active);


  const doneDuty = done.map(duty=> <Duty key={duty.id} duty={duty}
    delete={props.delete} change={props.change}/>)

    const activeDuties = active.map(duty=> <Duty key={duty.id} duty={duty}
      delete={props.delete} change={props.change}/>)
    
        return ( 
            <>
            <div class="active">
            <h2> Schedule of duties</h2>
              {activeDuties}
            </div>

            
             <div class="done">
             <h2> Done tasks</h2>
              {doneDuty.slice(0,5)}
            </div>
              </>
             );
    }
     
    export default Duties;
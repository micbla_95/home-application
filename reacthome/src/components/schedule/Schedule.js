import React, {Component} from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import AddDuty from './AddDuty';
import Duties from './Duties';
import Navigation from '../Navigation.js';

class Schedule extends Component {

   counter = 0;
  state = { 
      duties:[
         

       
      ]
   }


   deleteDuty=(id)=>{
      const duties = [...this.state.duties];
      const index = duties.findIndex(duty =>duty.id === id);
      duties.splice(index,1);

      this.setState({
          duties
      })
   }


  
   addDuty=(text,text2,date)=>{
     const duty={
      id:this.counter,
      text: text,
      text2: text2,
      date: date,
      active:true,
      finishDate: null

     }
     this.counter++

      this.setState(prevState=>({
        duties:[...prevState.duties, duty]
      }))
     return true
   }



   changeDutystatus=(id)=>{
    const duties = Array.from(this.state.duties);
      duties.forEach(duty =>{
        if (duty.id === id){
          duty.active = false;
          duty.finishDate = new Date().getTime()
        }
      })
      this.setState({
        duties
      })
  
     }


  render() { 
    return ( 
      <Router>
    
       <div class="TasksPanel">
         <AddDuty add= {this.addDuty}/>
         <Duties duties={this.state.duties} 
         delete={this.deleteDuty}
        change={this.changeDutystatus}
         />
        
       </div>
      </Router>
     );
  }
}
 
export default Schedule;
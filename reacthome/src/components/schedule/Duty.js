import React from 'react';

const Duty = (props) => {

    const {text,id, text2,date}=props.duty
    return ( 
        <div>
           <p>
               {text} {text2}  do dnia : {date}
               <button onClick={()=>props.delete(id)}>X</button>       
               <button onClick={()=>props.change(id)}>Done</button>
           </p>
        </div>
     );
}
 
export default Duty;
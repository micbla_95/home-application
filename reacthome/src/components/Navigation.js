import React from 'react';
import { NavLink, Switch, Route } from 'react-router-dom';
import '../styles/Navigation.css';
import ToDo from './to_do_list/Todo';

const navigation = [
  { name: "Split", path: "/", exact: true },
  { name: "Product", path: "/products" },
  { name: "Schedule", path: "/contact" },
  { name: "About", path: "/admin" },
  { name: "Log out", path: "/logout" },
 
]






const Navigation = () => {
  const handleRefresh=()=>{
   // window.location.reload()
   console.log("xx")
  }

  return ( 
   <nav className="navbarVisible">
        <a className="navbar-brand" href="#">
         
        </a>
        <div className="links-navbar-div">
          <div className="btn-nav">
            <ul className="nav navbar-nav navbar-right">
              <li>
                <NavLink to="/"></NavLink>
              </li>
              <li>
                <NavLink to="/productList/ProductPanel.js">Product</NavLink>
              </li>
              <li>
                <NavLink to="/to_do_list/Todo.js">To do</NavLink>
              </li>
              <li>
                <NavLink to="/split/SplitPanel.js">SplitPanel</NavLink>
              </li>
              <li>
                <NavLink to="/schedule/Schedule.js">Schedule</NavLink>
              </li>
              <li>
                <NavLink to="/">
                <button onClick={handleRefresh}>Log out</button>
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
   );
}

export default Navigation;
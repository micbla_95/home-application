import React, {Component} from 'react';
import './AddTask.css';
import axios from 'axios'

class AddTask extends Component {
    minDate = new Date().toISOString().slice(0,10);
    state = { 
        text: '',
        date:this.minDate

     }

     handleText=(e)=>{
         this.setState({
             text:e.target.value
         })
     }
     handleDate = (e) =>{
        this.setState({
            date: e.target.value
        })
     }

     handleClick= (e)=>{
        e.preventDefault()

        const tasks={
            task: this.state.text,
            date: this.state.date
            
         }
         
         axios.post('http://localhost:4000/app3/task', tasks)
         .then(response3 => console.log(response3.data))
         const {text,date}= this.state
         const add = this.props.add(text,date)
         if(add){
             this.setState({
                 text: '',
                 date: this.minDate
             })
         }
     }

    render() { 
        let maxDate = this.minDate.slice(0, 4)*1 +1;
        maxDate = maxDate + "-12-31";
     
        return (
            <div className="form">
               <input type="text" placeholder="dodaj zadnaie" 
               value={this.state.text} onChange={this.handleText}/>
               <label htmlFor="date">Do kiedy zrobić</label>
               < input type="date" value={this.state.date} min= {this.minDate} 
               max={maxDate} onChange={this.handleDate}/>
               <button onClick={this.handleClick}>Add</button>

            </div> 
         );
    }
}
 
export default AddTask;
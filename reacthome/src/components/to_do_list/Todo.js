import React, {Component} from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import AddTask from './AddTask';
import TaskList from './TaskList';
import './Todo.css'
import Navigation from '../Navigation.js';
class Todo extends Component {

   counter = 0;
  state = { 
      tasks:[
         

       
      ]
   }


   deleteTask=(id)=>{
      const tasks = [...this.state.tasks];
      const index = tasks.findIndex(task =>task.id === id);
      tasks.splice(index,1);

      this.setState({
        tasks
      })
   }

   changeTaskstatus=(id)=>{
  const tasks = Array.from(this.state.tasks);
    tasks.forEach(task =>{
      if (task.id === id){
        task.active = false;
        task.finishDate = new Date().getTime()
      }
    })
    this.setState({
      tasks
    })

   }
  
   addTask=(text, date)=>{
     const task={
      id:this.counter,
      text: text,
      date: date,
      active:true,
      finishDate: null

     }
     this.counter++

      this.setState(prevState=>({
        tasks:[...prevState.tasks, task]
      }))
     return true
   }
  render() { 
    return ( 
      <Router>
    
       <div class="TasksPanel">
         <AddTask add= {this.addTask}/>
         <TaskList tasks={this.state.tasks} 
         delete={this.deleteTask}
         change={this.changeTaskstatus}
         />
        
       </div>
      </Router>
     );
  }
}
 
export default Todo;
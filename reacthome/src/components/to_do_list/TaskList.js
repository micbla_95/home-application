import React from 'react';
import Task from './Task';
import './TaskList.css'

const TaskList = (props) => {
const active = props.tasks.filter(task => task.active);
const done = props.tasks.filter(task => !task.active);

  const activeTasks = active.map(task=> <Task key={task.id} task={task}
  delete={props.delete} change={props.change}/>)

  const doneTask = done.map(task=> <Task key={task.id} task={task}
    delete={props.delete} change={props.change}/>)
    
    return (
        <> 
        <div class="active">
           <h2> Task to do</h2>
          {activeTasks}
        </div>

        <div class="done">
        <h2> Done tasks</h2>
        {doneTask.slice(0,5)}
        </div>
        </>
     );
}
 
export default TaskList;
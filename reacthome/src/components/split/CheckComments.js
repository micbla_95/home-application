import React, {forwardRef, useImperativeHandle, useState} from "react";
import {motion, AnimatePresence} from "framer-motion"

const CheckComments = forwardRef((props,ref) => {

  const [open, setOpen] = useState(true)
  useImperativeHandle(ref, ()=>{
    return{
      open:()=>setOpen(true),
      close:()=>setOpen(false)
    }
  })
  return ( 
    <AnimatePresence>
      {open &&(
        <>
        <motion.div 
        initial={{
          opacity:0
        }}
    
        animate={{
          opacity:1,
          transition:{
            duration: 1.5
          }
        }}

        exit={{
            opacity:0
        }}

        onClick={()=> setOpen(false)}
        className="modal-backdrop" />
        <motion.div 
        initial={{
          scale:0
        }}
    
        animate={{
          scale:1.5,
          transition:{
            duration: 1
          }

        }}

        exit={{
          scale:0
      }}
        className="modal-content-wrapper" >
          <motion.div className="modal-content"
            initial={{
              x:100
            }}
        
            animate={{
               x:0
            }}
              
          exit={{
            x:100,
            opacity:0
        }} 
          
          >  {props.children}</motion.div>
        </motion.div>

        
        </>
      )}
    </AnimatePresence>
   );
}


)
 
export default CheckComments;
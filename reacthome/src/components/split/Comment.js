import React from 'react';

const Comment = (props) => {

    const {commentvalue,id}=props.bill
    return ( 
        <div>
           <p>
               {commentvalue} 
            
               <button onClick={()=>props.delete(id)}>X</button>
           </p>
        </div>
     );
}
 
export default Comment;
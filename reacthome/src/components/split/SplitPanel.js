import React, {Component} from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import AddBill from './AddBill';
import BillsList from './BillsList';
import Navigation from '../Navigation.js';

class SplitPanel extends Component {

   counter = 0;
   commentCounter=0;
  state = { 
      bills:[
         

       
      ],

      comments:[

      ]
   }


   deleteBills=(id)=>{
      const bills = [...this.state.bills];
      const index = bills.findIndex(bill =>bill.id === id);
      bills.splice(index,1);

      this.setState({
        bills
      })
   }

   changeBillstatus=(id)=>{
  const bills = Array.from(this.state.bills);
    bills.forEach(bill =>{
      if (bill.id === id){
        bill.active = false;
        bill.finishDate = new Date().getTime()
      }
    })
    this.setState({
      bills
    })

   }
  
   addBill=(text,text2, date,komentarz, BillPerUser)=>{
     const bill={
      id:this.counter,
      text: text,
      text2:text2,
      date: date,
      komentarz:komentarz,
      BillPerUser: BillPerUser,
      active:true,
      finishDate: null
    }
     this.counter++

      this.setState(prevState=>({
        bills:[...prevState.bills, bill]
      }))
     return true
   }

   addComment=(commentvalue)=>{
    
    const comment={
      id:this.commentCounter,
      commentvalue:commentvalue
    }
    this.commentCounter++
    this.setState(prevState=>({
      comments:[...prevState.comments, comment]
    }))
   return true
   }


  render() { 
    return ( 
      <Router>
    
       <div class="SplitPanel">
         <AddBill add= {this.addBill} commentvalue={this.addComment} bills={this.state.bills} />
         <BillsList bills={this.state.bills} 
         delete={this.deleteBills}
         change={this.changeBillstatus}
        
         />
        
       </div>
      </Router>
     );
  }
}
 
export default SplitPanel;
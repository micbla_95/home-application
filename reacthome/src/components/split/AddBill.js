import React, {Component, useRef} from 'react';
import Dialog from "./Dialog"; 
import SplitMembers from "./SplitMembers"; 

class AddBill extends Component {



 constructor(props) {
    super(props);
    this. modalRef = React.createRef();
    this. modalRef2 = React.createRef();
   
  }

    minDate = new Date().toISOString().slice(0,10);
    state = { 
        text: '',
        tex2: '',
        comment: ' ',
        komentarz:[],
        BillPerUser:0,
        isOpen: false,
        date:this.minDate,
        
      
     }



     handleText=(e)=>{
         this.setState({
             text:e.target.value
         })
     }

     handleText2=(e)=>{
        this.setState({
            text2:e.target.value
        })
    }

  



     handleDate = (e) =>{
        this.setState({
            date: e.target.value
        })
     }

     handleClick= ()=>{
         const {text,text2,date,komentarz,BillPerUser}= this.state
         const add = this.props.add(text,text2,date,komentarz,BillPerUser)
        
         if(add){
             this.setState({
                 text: '',
                 text2:'',
                 komentarz:'',
                 
               date: this.minDate
             })
         }

        
     }


     handleComment=(e)=>{
        this.setState({
            comment:e.target.value
        })

     }


     handleCommentClick=(e)=>{
   

        this.setState(prevState=>({
            komentarz:[...prevState.komentarz, this.state.comment+" "],
            comment:''
          }))
         return true
       
     }




     handleEditNote=()=>{
         console.log("xxx1")
         this.setState(prevState=>({
            comment:prevState.komentarz,
            komentarz:''
            
          }))
         
     }












     handleClickComment=(e)=>{
        const {commentvalue}= this.state
        
        const addComment = this.props.addComment(commentvalue)
        if(addComment){
            this.setState({
                commentvalue:''
            })
        }
     }


    render() { 
        //const re = new RegExp("^[0-9]+")
        const re = /^[0-9\b]+$/;
      
        let maxDate = this.minDate.slice(0, 4)*1 +1;
        maxDate = maxDate + "-12-31";
     
        return (
            <div className="form">
            
              
               <input type="text" placeholder="Co" 
               value={this.state.text} onChange={this.handleText}/>

                <input type="number" placeholder="za ile" 
               value={this.state.text2} onChange={this.handleText2}/>
                
                            
                <div className="infos">
                    <button disabled={(this.state.text.length <1 ) || (this.state.text2 !== '12' )} onClick={()=> this.modalRef.current.open()}>Dodaj szczegoły</button>
                    <Dialog ref={this.modalRef}>dodaj szczegóły zakupów
                       
                        <input type="text" placeholder="szczegoly"
                         value={this.state.comment} onChange={this.handleComment}/>
                        
                        <button onClick={this.handleCommentClick} >Add</button>
                        <button>Add receip photo</button>
                        <button onClick={this.handleEditNote}>Edit note</button>

                        <div class="Comments">
                       
                            {this.state.komentarz}
                        </div>
                    </Dialog>
               
                </div>


                <div className="members">
                    <button disabled={(this.state.text.length <1 ) || (this.state.text2 !== '12' )} onClick={()=> this.modalRef2.current.open()}>Dodaj na ile osob</button>
                    <SplitMembers ref={this.modalRef2}>dodaj ilosc osob do rachunku
                       
                        <input type="number" placeholder="ilosc osób"/>
                        

                    </SplitMembers>
               
                </div>

      
               <label htmlFor="date"> Data dodania</label>
               < input type="date" value={this.state.date} min= {this.minDate} 
               max={maxDate} onChange={this.handleDate}/>
               
               <button onClick={this.handleClick}>Add</button>

            </div> 
         );
    }
}
 
export default AddBill;
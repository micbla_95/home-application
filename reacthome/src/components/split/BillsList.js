import Bill from './Bill'
import SplitPanel from './SplitPanel';

 
const BillsList = (props) => {
    const active = props.bills.filter(bill => bill.active);
    const done = props.bills.filter(bill => !bill.active);


  const doneBill = done.map(bill=> <Bill key={bill.id} bill={bill}
    delete={props.delete} change={props.change} comments={props.comments}/>)

    const activeBill = active.map(bill=> <Bill key={bill.id} bill={bill}
      delete={props.delete} change={props.change}/>)
    
   let UserBill = props.bills.filter(bill=>bill.BillPerUser)

        return ( 
            <>
            <div class="active">
            <h2> Rachunki do rozliczenia</h2>
              {activeBill}
            </div>

             <div class="done">
             <h2> Historia : Transakcje rozliczone</h2>
              {doneBill.slice(0,5)}
            </div>

            <div class="Sumborrow">
             <h2> Podsumowanie : moje długi</h2>
           
            </div>


            <div class="Sumlet">
             <h2> Podsumowanie : Moje należności</h2>
             <p>user1 {UserBill}</p>
             <p>user2</p>
             <p>user3</p>          
            </div>
              </>
             );
    }
     
    export default BillsList;